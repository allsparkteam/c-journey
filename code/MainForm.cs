﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace c_journey
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void btnObjects_Click(object sender, EventArgs e)
        {
            var frm = new Objects.ObjectsForm();
            frm.Show();
        }

        private void btnVehicles_Click(object sender, EventArgs e)
        {
            var frm = new Objects.Vehicles();
            frm.Show();
        }

        private void btnAnimals_Click(object sender, EventArgs e)
        {
            var frm = new Objects.Animals();
            frm.Show();
        }

        private void btnOpenAnimalsAlternative_Click(object sender, EventArgs e)
        {
            var frm = new Objects.AnimalAlternative();
            frm.Show();
        }
    }
}
