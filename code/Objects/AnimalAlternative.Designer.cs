﻿
namespace c_journey.Objects
{
    partial class AnimalAlternative
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnMousePeep = new System.Windows.Forms.Button();
            this.btnMouseSqueak = new System.Windows.Forms.Button();
            this.btnDogWoof = new System.Windows.Forms.Button();
            this.btnDogWooof = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.btnCatMeow = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.btnCatHonk = new System.Windows.Forms.Button();
            this.lblCatAnswerResponse = new System.Windows.Forms.Label();
            this.lblDogAnswerResponse = new System.Windows.Forms.Label();
            this.lblMouseAnswerResponse = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(319, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Animal quiz ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 160);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(188, 13);
            this.label4.TabIndex = 26;
            this.label4.Text = "And what sound does a mouse make?";
            // 
            // btnMousePeep
            // 
            this.btnMousePeep.Location = new System.Drawing.Point(291, 155);
            this.btnMousePeep.Name = "btnMousePeep";
            this.btnMousePeep.Size = new System.Drawing.Size(75, 23);
            this.btnMousePeep.TabIndex = 25;
            this.btnMousePeep.Tag = "Peep";
            this.btnMousePeep.Text = "It peeps";
            this.btnMousePeep.UseVisualStyleBackColor = true;
            // 
            // btnMouseSqueak
            // 
            this.btnMouseSqueak.Location = new System.Drawing.Point(210, 155);
            this.btnMouseSqueak.Name = "btnMouseSqueak";
            this.btnMouseSqueak.Size = new System.Drawing.Size(75, 23);
            this.btnMouseSqueak.TabIndex = 24;
            this.btnMouseSqueak.Tag = "Squeak";
            this.btnMouseSqueak.Text = "It squeaks";
            this.btnMouseSqueak.UseVisualStyleBackColor = true;
            // 
            // btnDogWoof
            // 
            this.btnDogWoof.Location = new System.Drawing.Point(291, 121);
            this.btnDogWoof.Name = "btnDogWoof";
            this.btnDogWoof.Size = new System.Drawing.Size(75, 23);
            this.btnDogWoof.TabIndex = 23;
            this.btnDogWoof.Tag = "Woof";
            this.btnDogWoof.Text = "Woof";
            this.btnDogWoof.UseVisualStyleBackColor = true;
            // 
            // btnDogWooof
            // 
            this.btnDogWooof.Location = new System.Drawing.Point(210, 121);
            this.btnDogWooof.Name = "btnDogWooof";
            this.btnDogWooof.Size = new System.Drawing.Size(75, 23);
            this.btnDogWooof.TabIndex = 22;
            this.btnDogWooof.Tag = "Wooof";
            this.btnDogWooof.Text = "Wooof";
            this.btnDogWooof.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(48, 126);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(156, 13);
            this.label3.TabIndex = 21;
            this.label3.Text = "What sound does a dog make?";
            // 
            // btnCatMeow
            // 
            this.btnCatMeow.Location = new System.Drawing.Point(291, 86);
            this.btnCatMeow.Name = "btnCatMeow";
            this.btnCatMeow.Size = new System.Drawing.Size(75, 23);
            this.btnCatMeow.TabIndex = 20;
            this.btnCatMeow.Tag = "Meow";
            this.btnCatMeow.Text = "Meow";
            this.btnCatMeow.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(51, 91);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(153, 13);
            this.label2.TabIndex = 19;
            this.label2.Text = "What sound does a cat make?";
            // 
            // btnCatHonk
            // 
            this.btnCatHonk.Location = new System.Drawing.Point(210, 86);
            this.btnCatHonk.Name = "btnCatHonk";
            this.btnCatHonk.Size = new System.Drawing.Size(75, 23);
            this.btnCatHonk.TabIndex = 18;
            this.btnCatHonk.Tag = "Honk";
            this.btnCatHonk.Text = "Honk";
            this.btnCatHonk.UseVisualStyleBackColor = true;
            // 
            // lblCatAnswerResponse
            // 
            this.lblCatAnswerResponse.AutoSize = true;
            this.lblCatAnswerResponse.Location = new System.Drawing.Point(372, 91);
            this.lblCatAnswerResponse.Name = "lblCatAnswerResponse";
            this.lblCatAnswerResponse.Size = new System.Drawing.Size(112, 13);
            this.lblCatAnswerResponse.TabIndex = 27;
            this.lblCatAnswerResponse.Text = "Click button to answer";
            this.lblCatAnswerResponse.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDogAnswerResponse
            // 
            this.lblDogAnswerResponse.AutoSize = true;
            this.lblDogAnswerResponse.Location = new System.Drawing.Point(372, 126);
            this.lblDogAnswerResponse.Name = "lblDogAnswerResponse";
            this.lblDogAnswerResponse.Size = new System.Drawing.Size(112, 13);
            this.lblDogAnswerResponse.TabIndex = 28;
            this.lblDogAnswerResponse.Text = "Click button to answer";
            this.lblDogAnswerResponse.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMouseAnswerResponse
            // 
            this.lblMouseAnswerResponse.AutoSize = true;
            this.lblMouseAnswerResponse.Location = new System.Drawing.Point(372, 160);
            this.lblMouseAnswerResponse.Name = "lblMouseAnswerResponse";
            this.lblMouseAnswerResponse.Size = new System.Drawing.Size(112, 13);
            this.lblMouseAnswerResponse.TabIndex = 29;
            this.lblMouseAnswerResponse.Text = "Click button to answer";
            this.lblMouseAnswerResponse.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // AnimalAlternative
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.lblMouseAnswerResponse);
            this.Controls.Add(this.lblDogAnswerResponse);
            this.Controls.Add(this.lblCatAnswerResponse);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnMousePeep);
            this.Controls.Add(this.btnMouseSqueak);
            this.Controls.Add(this.btnDogWoof);
            this.Controls.Add(this.btnDogWooof);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btnCatMeow);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnCatHonk);
            this.Controls.Add(this.label1);
            this.Name = "AnimalAlternative";
            this.Text = "AnimalAlternative";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnMousePeep;
        private System.Windows.Forms.Button btnMouseSqueak;
        private System.Windows.Forms.Button btnDogWoof;
        private System.Windows.Forms.Button btnDogWooof;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnCatMeow;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnCatHonk;
        private System.Windows.Forms.Label lblCatAnswerResponse;
        private System.Windows.Forms.Label lblDogAnswerResponse;
        private System.Windows.Forms.Label lblMouseAnswerResponse;
    }
}