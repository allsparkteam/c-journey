﻿
namespace c_journey.Objects
{
    partial class Vehicles
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnNewBicycle = new System.Windows.Forms.Button();
            this.btnNewCar = new System.Windows.Forms.Button();
            this.txtBicycleType = new System.Windows.Forms.TextBox();
            this.txtBicycleColor = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.numBicycleAverageSpeed = new System.Windows.Forms.NumericUpDown();
            this.txtCarType = new System.Windows.Forms.TextBox();
            this.txtCarModel = new System.Windows.Forms.TextBox();
            this.txtCarColor = new System.Windows.Forms.TextBox();
            this.txtCarOwner = new System.Windows.Forms.TextBox();
            this.numCarYearBuilt = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtBicycleMaker = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtBicycleTire = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numBicycleAverageSpeed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numCarYearBuilt)).BeginInit();
            this.SuspendLayout();
            // 
            // btnNewBicycle
            // 
            this.btnNewBicycle.Location = new System.Drawing.Point(268, 111);
            this.btnNewBicycle.Name = "btnNewBicycle";
            this.btnNewBicycle.Size = new System.Drawing.Size(75, 23);
            this.btnNewBicycle.TabIndex = 0;
            this.btnNewBicycle.Text = "New Bicycle";
            this.btnNewBicycle.UseVisualStyleBackColor = true;
            this.btnNewBicycle.Click += new System.EventHandler(this.btnNewBicycle_Click);
            // 
            // btnNewCar
            // 
            this.btnNewCar.Location = new System.Drawing.Point(457, 115);
            this.btnNewCar.Name = "btnNewCar";
            this.btnNewCar.Size = new System.Drawing.Size(75, 23);
            this.btnNewCar.TabIndex = 1;
            this.btnNewCar.Text = "New Car";
            this.btnNewCar.UseVisualStyleBackColor = true;
            this.btnNewCar.Click += new System.EventHandler(this.btnNewCar_Click);
            // 
            // txtBicycleType
            // 
            this.txtBicycleType.Location = new System.Drawing.Point(252, 167);
            this.txtBicycleType.Name = "txtBicycleType";
            this.txtBicycleType.Size = new System.Drawing.Size(100, 20);
            this.txtBicycleType.TabIndex = 2;
            // 
            // txtBicycleColor
            // 
            this.txtBicycleColor.Location = new System.Drawing.Point(252, 193);
            this.txtBicycleColor.Name = "txtBicycleColor";
            this.txtBicycleColor.Size = new System.Drawing.Size(100, 20);
            this.txtBicycleColor.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(211, 170);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Type";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(207, 196);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Color";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(176, 224);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Average mph";
            // 
            // numBicycleAverageSpeed
            // 
            this.numBicycleAverageSpeed.Location = new System.Drawing.Point(252, 222);
            this.numBicycleAverageSpeed.Name = "numBicycleAverageSpeed";
            this.numBicycleAverageSpeed.Size = new System.Drawing.Size(100, 20);
            this.numBicycleAverageSpeed.TabIndex = 8;
            // 
            // txtCarType
            // 
            this.txtCarType.Location = new System.Drawing.Point(445, 144);
            this.txtCarType.Name = "txtCarType";
            this.txtCarType.Size = new System.Drawing.Size(111, 20);
            this.txtCarType.TabIndex = 9;
            // 
            // txtCarModel
            // 
            this.txtCarModel.Location = new System.Drawing.Point(445, 170);
            this.txtCarModel.Name = "txtCarModel";
            this.txtCarModel.Size = new System.Drawing.Size(111, 20);
            this.txtCarModel.TabIndex = 10;
            // 
            // txtCarColor
            // 
            this.txtCarColor.Location = new System.Drawing.Point(445, 196);
            this.txtCarColor.Name = "txtCarColor";
            this.txtCarColor.Size = new System.Drawing.Size(111, 20);
            this.txtCarColor.TabIndex = 11;
            // 
            // txtCarOwner
            // 
            this.txtCarOwner.Location = new System.Drawing.Point(445, 248);
            this.txtCarOwner.Name = "txtCarOwner";
            this.txtCarOwner.Size = new System.Drawing.Size(111, 20);
            this.txtCarOwner.TabIndex = 13;
            // 
            // numCarYearBuilt
            // 
            this.numCarYearBuilt.Location = new System.Drawing.Point(445, 222);
            this.numCarYearBuilt.Maximum = new decimal(new int[] {
            2011,
            0,
            0,
            0});
            this.numCarYearBuilt.Name = "numCarYearBuilt";
            this.numCarYearBuilt.Size = new System.Drawing.Size(111, 20);
            this.numCarYearBuilt.TabIndex = 14;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(404, 147);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "Type";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(400, 173);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(36, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "Model";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(400, 201);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(31, 13);
            this.label6.TabIndex = 17;
            this.label6.Text = "Color";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(388, 229);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(51, 13);
            this.label7.TabIndex = 18;
            this.label7.Text = "Year built";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(404, 251);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(38, 13);
            this.label8.TabIndex = 19;
            this.label8.Text = "Owner";
            // 
            // txtBicycleMaker
            // 
            this.txtBicycleMaker.Location = new System.Drawing.Point(252, 141);
            this.txtBicycleMaker.Name = "txtBicycleMaker";
            this.txtBicycleMaker.Size = new System.Drawing.Size(100, 20);
            this.txtBicycleMaker.TabIndex = 20;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(211, 144);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(37, 13);
            this.label9.TabIndex = 21;
            this.label9.Text = "Maker";
            // 
            // txtBicycleTire
            // 
            this.txtBicycleTire.Location = new System.Drawing.Point(252, 248);
            this.txtBicycleTire.Name = "txtBicycleTire";
            this.txtBicycleTire.Size = new System.Drawing.Size(100, 20);
            this.txtBicycleTire.TabIndex = 22;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(188, 251);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(60, 13);
            this.label10.TabIndex = 23;
            this.label10.Text = "Type of tire";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(344, 348);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 24;
            this.button1.Text = "Get display";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Vehicles
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtBicycleTire);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtBicycleMaker);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.numCarYearBuilt);
            this.Controls.Add(this.txtCarOwner);
            this.Controls.Add(this.txtCarColor);
            this.Controls.Add(this.txtCarModel);
            this.Controls.Add(this.txtCarType);
            this.Controls.Add(this.numBicycleAverageSpeed);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtBicycleColor);
            this.Controls.Add(this.txtBicycleType);
            this.Controls.Add(this.btnNewCar);
            this.Controls.Add(this.btnNewBicycle);
            this.Name = "Vehicles";
            this.Text = "Vehicles";
            ((System.ComponentModel.ISupportInitialize)(this.numBicycleAverageSpeed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numCarYearBuilt)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnNewBicycle;
        private System.Windows.Forms.Button btnNewCar;
        private System.Windows.Forms.TextBox txtBicycleType;
        private System.Windows.Forms.TextBox txtBicycleColor;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown numBicycleAverageSpeed;
        private System.Windows.Forms.TextBox txtCarType;
        private System.Windows.Forms.TextBox txtCarModel;
        private System.Windows.Forms.TextBox txtCarColor;
        private System.Windows.Forms.TextBox txtCarOwner;
        private System.Windows.Forms.NumericUpDown numCarYearBuilt;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtBicycleMaker;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtBicycleTire;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button button1;
    }
}