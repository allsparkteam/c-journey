﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace c_journey.Objects
{
    public partial class AnimalAlternative : Form
    {
        public AnimalAlternative()
        {
            InitializeComponent();
            this.btnCatHonk.Click += new System.EventHandler(this.btnCatAnswer_Click);
            this.btnCatMeow.Click += new System.EventHandler(this.btnCatAnswer_Click);
            this.btnDogWoof.Click += new System.EventHandler(this.btnDogAnswer_Click);
            this.btnDogWooof.Click += new System.EventHandler(this.btnDogAnswer_Click);
            this.btnMousePeep.Click += new System.EventHandler(this.btnMouseAnswer_Click);
            this.btnMouseSqueak.Click += new System.EventHandler(this.btnMouseAnswer_Click);

        }

        private void btnCatAnswer_Click(object sender, EventArgs e)
        {

            var sound = (sender as Button).Tag.ToString();
            var animal = new Data.Cat();
            
            lblCatAnswerResponse.Text = $"{animal.CheckSound(sound)}";

        }
        private void btnDogAnswer_Click(object sender, EventArgs e)
        {

            var sound = (sender as Button).Tag.ToString();
            var animal = new Data.Dog();
            lblDogAnswerResponse.Text = $"{animal.CheckSound(sound)}";

        }
        private void btnMouseAnswer_Click(object sender, EventArgs e)
        {

            var sound = (sender as Button).Tag.ToString();
            var animal = new Data.Mouse();
            lblMouseAnswerResponse.Text = $"{animal.CheckSound(sound)}";

        }
    }
}
