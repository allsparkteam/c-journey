﻿
using System;

namespace c_journey.Objects
{
    partial class Animals
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtWrongCatSound = new System.Windows.Forms.TextBox();
            this.txtRightCatSound = new System.Windows.Forms.TextBox();
            this.btnWrongCatSound = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.btnRightCatSound = new System.Windows.Forms.Button();
            this.txtWrongDogSound = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtRightDogSound = new System.Windows.Forms.TextBox();
            this.btnDogWrongSound = new System.Windows.Forms.Button();
            this.btnRightDogSound = new System.Windows.Forms.Button();
            this.txtRightMouseSound = new System.Windows.Forms.TextBox();
            this.txtWrongMouseSound = new System.Windows.Forms.TextBox();
            this.btnRightMouseSound = new System.Windows.Forms.Button();
            this.btnWrongMouseSound = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnRightDogAnswer = new System.Windows.Forms.Button();
            this.btnWrongMouseAnswer = new System.Windows.Forms.Button();
            this.btnWrongCatAnswer = new System.Windows.Forms.Button();
            this.txtRightDogAnswer = new System.Windows.Forms.TextBox();
            this.txtWrongMouseAnswer = new System.Windows.Forms.TextBox();
            this.txtWrongCatAnswer = new System.Windows.Forms.TextBox();
            this.txtWrongCatAnswer2 = new System.Windows.Forms.TextBox();
            this.txtRightMouseAnswer = new System.Windows.Forms.TextBox();
            this.txtWrongDogAnswer2 = new System.Windows.Forms.TextBox();
            this.btnWrongCatAnswer2 = new System.Windows.Forms.Button();
            this.btnRightMouseAnswer = new System.Windows.Forms.Button();
            this.btnWrongDogAnswer2 = new System.Windows.Forms.Button();
            this.txtRightCatAnswer = new System.Windows.Forms.TextBox();
            this.txtWrongMouseAnswer2 = new System.Windows.Forms.TextBox();
            this.txtWrongDogAnswer3 = new System.Windows.Forms.TextBox();
            this.btnRightCatAnswer = new System.Windows.Forms.Button();
            this.btnWrongMouseAnswer2 = new System.Windows.Forms.Button();
            this.btnWrongDogAnswer3 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(377, 58);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Animal quiz ";
            // 
            // txtWrongCatSound
            // 
            this.txtWrongCatSound.Location = new System.Drawing.Point(269, 117);
            this.txtWrongCatSound.Name = "txtWrongCatSound";
            this.txtWrongCatSound.Size = new System.Drawing.Size(100, 20);
            this.txtWrongCatSound.TabIndex = 1;
            // 
            // txtRightCatSound
            // 
            this.txtRightCatSound.Location = new System.Drawing.Point(269, 143);
            this.txtRightCatSound.Name = "txtRightCatSound";
            this.txtRightCatSound.Size = new System.Drawing.Size(100, 20);
            this.txtRightCatSound.TabIndex = 3;
            // 
            // btnWrongCatSound
            // 
            this.btnWrongCatSound.Location = new System.Drawing.Point(188, 114);
            this.btnWrongCatSound.Name = "btnWrongCatSound";
            this.btnWrongCatSound.Size = new System.Drawing.Size(75, 23);
            this.btnWrongCatSound.TabIndex = 4;
            this.btnWrongCatSound.Text = "Honk";
            this.btnWrongCatSound.UseVisualStyleBackColor = true;
            this.btnWrongCatSound.Click += new System.EventHandler(this.btnCatSound1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(35, 127);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(153, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "What sound does a cat make?";
            // 
            // btnRightCatSound
            // 
            this.btnRightCatSound.Location = new System.Drawing.Point(188, 143);
            this.btnRightCatSound.Name = "btnRightCatSound";
            this.btnRightCatSound.Size = new System.Drawing.Size(75, 23);
            this.btnRightCatSound.TabIndex = 7;
            this.btnRightCatSound.Text = "Meow";
            this.btnRightCatSound.UseVisualStyleBackColor = true;
            this.btnRightCatSound.Click += new System.EventHandler(this.btnRightCatSound_Click);
            // 
            // txtWrongDogSound
            // 
            this.txtWrongDogSound.Location = new System.Drawing.Point(269, 212);
            this.txtWrongDogSound.Name = "txtWrongDogSound";
            this.txtWrongDogSound.Size = new System.Drawing.Size(100, 20);
            this.txtWrongDogSound.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(26, 220);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(156, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "What sound does a dog make?";
            // 
            // txtRightDogSound
            // 
            this.txtRightDogSound.Location = new System.Drawing.Point(269, 238);
            this.txtRightDogSound.Name = "txtRightDogSound";
            this.txtRightDogSound.Size = new System.Drawing.Size(100, 20);
            this.txtRightDogSound.TabIndex = 10;
            // 
            // btnDogWrongSound
            // 
            this.btnDogWrongSound.Location = new System.Drawing.Point(188, 210);
            this.btnDogWrongSound.Name = "btnDogWrongSound";
            this.btnDogWrongSound.Size = new System.Drawing.Size(75, 23);
            this.btnDogWrongSound.TabIndex = 11;
            this.btnDogWrongSound.Text = "Wooof";
            this.btnDogWrongSound.UseVisualStyleBackColor = true;
            this.btnDogWrongSound.Click += new System.EventHandler(this.btnDogRightAnswer_Click);
            // 
            // btnRightDogSound
            // 
            this.btnRightDogSound.Location = new System.Drawing.Point(188, 236);
            this.btnRightDogSound.Name = "btnRightDogSound";
            this.btnRightDogSound.Size = new System.Drawing.Size(75, 23);
            this.btnRightDogSound.TabIndex = 12;
            this.btnRightDogSound.Text = "Woof";
            this.btnRightDogSound.UseVisualStyleBackColor = true;
            this.btnRightDogSound.Click += new System.EventHandler(this.btnRightDogSound_Click);
            // 
            // txtRightMouseSound
            // 
            this.txtRightMouseSound.Location = new System.Drawing.Point(269, 299);
            this.txtRightMouseSound.Name = "txtRightMouseSound";
            this.txtRightMouseSound.Size = new System.Drawing.Size(100, 20);
            this.txtRightMouseSound.TabIndex = 13;
            // 
            // txtWrongMouseSound
            // 
            this.txtWrongMouseSound.Location = new System.Drawing.Point(269, 325);
            this.txtWrongMouseSound.Name = "txtWrongMouseSound";
            this.txtWrongMouseSound.Size = new System.Drawing.Size(100, 20);
            this.txtWrongMouseSound.TabIndex = 14;
            // 
            // btnRightMouseSound
            // 
            this.btnRightMouseSound.Location = new System.Drawing.Point(188, 299);
            this.btnRightMouseSound.Name = "btnRightMouseSound";
            this.btnRightMouseSound.Size = new System.Drawing.Size(75, 23);
            this.btnRightMouseSound.TabIndex = 15;
            this.btnRightMouseSound.Text = "It squeaks";
            this.btnRightMouseSound.UseVisualStyleBackColor = true;
            this.btnRightMouseSound.Click += new System.EventHandler(this.btnRightMouseSound_Click);
            // 
            // btnWrongMouseSound
            // 
            this.btnWrongMouseSound.Location = new System.Drawing.Point(188, 325);
            this.btnWrongMouseSound.Name = "btnWrongMouseSound";
            this.btnWrongMouseSound.Size = new System.Drawing.Size(75, 23);
            this.btnWrongMouseSound.TabIndex = 16;
            this.btnWrongMouseSound.Text = "It peeps";
            this.btnWrongMouseSound.UseVisualStyleBackColor = true;
            this.btnWrongMouseSound.Click += new System.EventHandler(this.btnMouseWrongSound_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(0, 325);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(188, 13);
            this.label4.TabIndex = 17;
            this.label4.Text = "And what sound does a mouse make?";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(466, 101);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(297, 13);
            this.label5.TabIndex = 18;
            this.label5.Text = "What is loud, usually very friendly and likes walks and bones?";
            // 
            // btnRightDogAnswer
            // 
            this.btnRightDogAnswer.Location = new System.Drawing.Point(469, 122);
            this.btnRightDogAnswer.Name = "btnRightDogAnswer";
            this.btnRightDogAnswer.Size = new System.Drawing.Size(75, 23);
            this.btnRightDogAnswer.TabIndex = 19;
            this.btnRightDogAnswer.Text = "A Dog";
            this.btnRightDogAnswer.UseVisualStyleBackColor = true;
            this.btnRightDogAnswer.Click += new System.EventHandler(this.btnRightDogAnswer_Click);
            // 
            // btnWrongMouseAnswer
            // 
            this.btnWrongMouseAnswer.Location = new System.Drawing.Point(561, 122);
            this.btnWrongMouseAnswer.Name = "btnWrongMouseAnswer";
            this.btnWrongMouseAnswer.Size = new System.Drawing.Size(75, 23);
            this.btnWrongMouseAnswer.TabIndex = 20;
            this.btnWrongMouseAnswer.Text = "A Mouse";
            this.btnWrongMouseAnswer.UseVisualStyleBackColor = true;
            this.btnWrongMouseAnswer.Click += new System.EventHandler(this.btnWrongMouseAnswer_Click);
            // 
            // btnWrongCatAnswer
            // 
            this.btnWrongCatAnswer.Location = new System.Drawing.Point(652, 122);
            this.btnWrongCatAnswer.Name = "btnWrongCatAnswer";
            this.btnWrongCatAnswer.Size = new System.Drawing.Size(75, 23);
            this.btnWrongCatAnswer.TabIndex = 21;
            this.btnWrongCatAnswer.Text = "A Cat";
            this.btnWrongCatAnswer.UseVisualStyleBackColor = true;
            this.btnWrongCatAnswer.Click += new System.EventHandler(this.btnWrongCatAnswer_Click);
            // 
            // txtRightDogAnswer
            // 
            this.txtRightDogAnswer.Location = new System.Drawing.Point(469, 151);
            this.txtRightDogAnswer.Name = "txtRightDogAnswer";
            this.txtRightDogAnswer.Size = new System.Drawing.Size(75, 20);
            this.txtRightDogAnswer.TabIndex = 22;
            // 
            // txtWrongMouseAnswer
            // 
            this.txtWrongMouseAnswer.Location = new System.Drawing.Point(561, 151);
            this.txtWrongMouseAnswer.Name = "txtWrongMouseAnswer";
            this.txtWrongMouseAnswer.Size = new System.Drawing.Size(75, 20);
            this.txtWrongMouseAnswer.TabIndex = 23;
            // 
            // txtWrongCatAnswer
            // 
            this.txtWrongCatAnswer.Location = new System.Drawing.Point(652, 151);
            this.txtWrongCatAnswer.Name = "txtWrongCatAnswer";
            this.txtWrongCatAnswer.Size = new System.Drawing.Size(75, 20);
            this.txtWrongCatAnswer.TabIndex = 24;
            // 
            // txtWrongCatAnswer2
            // 
            this.txtWrongCatAnswer2.Location = new System.Drawing.Point(652, 249);
            this.txtWrongCatAnswer2.Name = "txtWrongCatAnswer2";
            this.txtWrongCatAnswer2.Size = new System.Drawing.Size(75, 20);
            this.txtWrongCatAnswer2.TabIndex = 30;
            // 
            // txtRightMouseAnswer
            // 
            this.txtRightMouseAnswer.Location = new System.Drawing.Point(561, 249);
            this.txtRightMouseAnswer.Name = "txtRightMouseAnswer";
            this.txtRightMouseAnswer.Size = new System.Drawing.Size(75, 20);
            this.txtRightMouseAnswer.TabIndex = 29;
            // 
            // txtWrongDogAnswer2
            // 
            this.txtWrongDogAnswer2.Location = new System.Drawing.Point(469, 249);
            this.txtWrongDogAnswer2.Name = "txtWrongDogAnswer2";
            this.txtWrongDogAnswer2.Size = new System.Drawing.Size(75, 20);
            this.txtWrongDogAnswer2.TabIndex = 28;
            // 
            // btnWrongCatAnswer2
            // 
            this.btnWrongCatAnswer2.Location = new System.Drawing.Point(652, 220);
            this.btnWrongCatAnswer2.Name = "btnWrongCatAnswer2";
            this.btnWrongCatAnswer2.Size = new System.Drawing.Size(75, 23);
            this.btnWrongCatAnswer2.TabIndex = 27;
            this.btnWrongCatAnswer2.Text = "A Cat";
            this.btnWrongCatAnswer2.UseVisualStyleBackColor = true;
            this.btnWrongCatAnswer2.Click += new System.EventHandler(this.btnWrongCatAnswer2_Click);
            // 
            // btnRightMouseAnswer
            // 
            this.btnRightMouseAnswer.Location = new System.Drawing.Point(561, 220);
            this.btnRightMouseAnswer.Name = "btnRightMouseAnswer";
            this.btnRightMouseAnswer.Size = new System.Drawing.Size(75, 23);
            this.btnRightMouseAnswer.TabIndex = 26;
            this.btnRightMouseAnswer.Text = "A Mouse";
            this.btnRightMouseAnswer.UseVisualStyleBackColor = true;
            this.btnRightMouseAnswer.Click += new System.EventHandler(this.btnMouseRightAnswer_Click);
            // 
            // btnWrongDogAnswer2
            // 
            this.btnWrongDogAnswer2.Location = new System.Drawing.Point(469, 220);
            this.btnWrongDogAnswer2.Name = "btnWrongDogAnswer2";
            this.btnWrongDogAnswer2.Size = new System.Drawing.Size(75, 23);
            this.btnWrongDogAnswer2.TabIndex = 25;
            this.btnWrongDogAnswer2.Text = "A Dog";
            this.btnWrongDogAnswer2.UseVisualStyleBackColor = true;
            this.btnWrongDogAnswer2.Click += new System.EventHandler(this.btnWrongDogAnswer2_Click);
            // 
            // txtRightCatAnswer
            // 
            this.txtRightCatAnswer.Location = new System.Drawing.Point(652, 354);
            this.txtRightCatAnswer.Name = "txtRightCatAnswer";
            this.txtRightCatAnswer.Size = new System.Drawing.Size(75, 20);
            this.txtRightCatAnswer.TabIndex = 36;
            // 
            // txtWrongMouseAnswer2
            // 
            this.txtWrongMouseAnswer2.Location = new System.Drawing.Point(561, 354);
            this.txtWrongMouseAnswer2.Name = "txtWrongMouseAnswer2";
            this.txtWrongMouseAnswer2.Size = new System.Drawing.Size(75, 20);
            this.txtWrongMouseAnswer2.TabIndex = 35;
            // 
            // txtWrongDogAnswer3
            // 
            this.txtWrongDogAnswer3.Location = new System.Drawing.Point(469, 354);
            this.txtWrongDogAnswer3.Name = "txtWrongDogAnswer3";
            this.txtWrongDogAnswer3.Size = new System.Drawing.Size(75, 20);
            this.txtWrongDogAnswer3.TabIndex = 34;
            // 
            // btnRightCatAnswer
            // 
            this.btnRightCatAnswer.Location = new System.Drawing.Point(652, 325);
            this.btnRightCatAnswer.Name = "btnRightCatAnswer";
            this.btnRightCatAnswer.Size = new System.Drawing.Size(75, 23);
            this.btnRightCatAnswer.TabIndex = 33;
            this.btnRightCatAnswer.Text = "A Cat";
            this.btnRightCatAnswer.UseVisualStyleBackColor = true;
            this.btnRightCatAnswer.Click += new System.EventHandler(this.btnRightCatAnswer_Click);
            // 
            // btnWrongMouseAnswer2
            // 
            this.btnWrongMouseAnswer2.Location = new System.Drawing.Point(561, 325);
            this.btnWrongMouseAnswer2.Name = "btnWrongMouseAnswer2";
            this.btnWrongMouseAnswer2.Size = new System.Drawing.Size(75, 23);
            this.btnWrongMouseAnswer2.TabIndex = 32;
            this.btnWrongMouseAnswer2.Text = "A Mouse";
            this.btnWrongMouseAnswer2.UseVisualStyleBackColor = true;
            this.btnWrongMouseAnswer2.Click += new System.EventHandler(this.btnWrongMouseAnswer2_Click);
            // 
            // btnWrongDogAnswer3
            // 
            this.btnWrongDogAnswer3.Location = new System.Drawing.Point(469, 325);
            this.btnWrongDogAnswer3.Name = "btnWrongDogAnswer3";
            this.btnWrongDogAnswer3.Size = new System.Drawing.Size(75, 23);
            this.btnWrongDogAnswer3.TabIndex = 31;
            this.btnWrongDogAnswer3.Text = "A Dog";
            this.btnWrongDogAnswer3.UseVisualStyleBackColor = true;
            this.btnWrongDogAnswer3.Click += new System.EventHandler(this.btnWrongDogAnswer3_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(449, 193);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(339, 13);
            this.label6.TabIndex = 37;
            this.label6.Text = "What is small, can live in your walls and something cats enjoy hunting?";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(449, 302);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(295, 13);
            this.label7.TabIndex = 38;
            this.label7.Text = "What likes fish, sleeps on your laptop and likes to climb stuff?";
            // 
            // Animals
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtRightCatAnswer);
            this.Controls.Add(this.txtWrongMouseAnswer2);
            this.Controls.Add(this.txtWrongDogAnswer3);
            this.Controls.Add(this.btnRightCatAnswer);
            this.Controls.Add(this.btnWrongMouseAnswer2);
            this.Controls.Add(this.btnWrongDogAnswer3);
            this.Controls.Add(this.txtWrongCatAnswer2);
            this.Controls.Add(this.txtRightMouseAnswer);
            this.Controls.Add(this.txtWrongDogAnswer2);
            this.Controls.Add(this.btnWrongCatAnswer2);
            this.Controls.Add(this.btnRightMouseAnswer);
            this.Controls.Add(this.btnWrongDogAnswer2);
            this.Controls.Add(this.txtWrongCatAnswer);
            this.Controls.Add(this.txtWrongMouseAnswer);
            this.Controls.Add(this.txtRightDogAnswer);
            this.Controls.Add(this.btnWrongCatAnswer);
            this.Controls.Add(this.btnWrongMouseAnswer);
            this.Controls.Add(this.btnRightDogAnswer);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnWrongMouseSound);
            this.Controls.Add(this.btnRightMouseSound);
            this.Controls.Add(this.txtWrongMouseSound);
            this.Controls.Add(this.txtRightMouseSound);
            this.Controls.Add(this.btnRightDogSound);
            this.Controls.Add(this.btnDogWrongSound);
            this.Controls.Add(this.txtRightDogSound);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtWrongDogSound);
            this.Controls.Add(this.btnRightCatSound);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnWrongCatSound);
            this.Controls.Add(this.txtRightCatSound);
            this.Controls.Add(this.txtWrongCatSound);
            this.Controls.Add(this.label1);
            this.Name = "Animals";
            this.Text = "Animals";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

  

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtWrongCatSound;
        private System.Windows.Forms.TextBox txtRightCatSound;
        private System.Windows.Forms.Button btnWrongCatSound;
        private System.Windows.Forms.Label label2;
        private EventHandler btnCatSqueak_Click;
        private System.Windows.Forms.Button btnRightCatSound;
        private System.Windows.Forms.TextBox txtWrongDogSound;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtRightDogSound;
        private System.Windows.Forms.Button btnDogWrongSound;
        private System.Windows.Forms.Button btnRightDogSound;
        private System.Windows.Forms.TextBox txtRightMouseSound;
        private System.Windows.Forms.TextBox txtWrongMouseSound;
        private System.Windows.Forms.Button btnRightMouseSound;
        private System.Windows.Forms.Button btnWrongMouseSound;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnRightDogAnswer;
        private System.Windows.Forms.Button btnWrongMouseAnswer;
        private System.Windows.Forms.Button btnWrongCatAnswer;
        private System.Windows.Forms.TextBox txtRightDogAnswer;
        private System.Windows.Forms.TextBox txtWrongMouseAnswer;
        private System.Windows.Forms.TextBox txtWrongCatAnswer;
        private System.Windows.Forms.TextBox txtWrongCatAnswer2;
        private System.Windows.Forms.TextBox txtRightMouseAnswer;
        private System.Windows.Forms.TextBox txtWrongDogAnswer2;
        private System.Windows.Forms.Button btnWrongCatAnswer2;
        private System.Windows.Forms.Button btnRightMouseAnswer;
        private System.Windows.Forms.Button btnWrongDogAnswer2;
        private System.Windows.Forms.TextBox txtRightCatAnswer;
        private System.Windows.Forms.TextBox txtWrongMouseAnswer2;
        private System.Windows.Forms.TextBox txtWrongDogAnswer3;
        private System.Windows.Forms.Button btnRightCatAnswer;
        private System.Windows.Forms.Button btnWrongMouseAnswer2;
        private System.Windows.Forms.Button btnWrongDogAnswer3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
    }
}