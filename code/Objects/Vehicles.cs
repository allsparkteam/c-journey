﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace c_journey.Objects
{
    public partial class Vehicles : Form
    {
        public Vehicles()
        {
            InitializeComponent();
        }

        private void btnNewBicycle_Click(object sender, EventArgs e)
        {
            var bicycle = CreateBicycle("Kona bicycles", "Fire Mountain", "Blue", 15, "Tubeless");

            txtBicycleType.Text = bicycle.Type;
            txtBicycleColor.Text = bicycle.Color;
            numBicycleAverageSpeed.Value = bicycle.AverageSpeed;
            txtBicycleMaker.Text = bicycle.Maker;
            txtBicycleTire.Text = bicycle.Tire;
        }

        private Data.Bicycle CreateBicycle(string maker, string type, string color, int averagespeed, string tire)
        {
            var bicycle = new Data.Bicycle();
            bicycle.Maker = maker;
            bicycle.Type = type;
            bicycle.Color = color;
            bicycle.AverageSpeed = averagespeed;
            bicycle.Tire = tire;

            return bicycle;
        }



        private void btnNewCar_Click(object sender, EventArgs e)
        {
            var person = CreatePerson("Bobb");
            var car = CreateCar("Sports car", "Lamborgini Aventador", "Dark blue", 2011,  person);


            txtCarType.Text = car.Type;
            txtCarModel.Text = car.Model;
            txtCarColor.Text = car.Color;
            numCarYearBuilt.Value = car.YearBuilt;
            txtCarOwner.Text = car.Owner.Name;
        }

        private Data.Car CreateCar(string type, string Model, string color, int yearbuilt, Data.Person owner)
        {
            var car = new Data.Car();
            car.Type = type;
            car.Model = Model;
            car.Color = color;
            car.YearBuilt = yearbuilt;
            car.Owner = owner;

            return car;
        }

        private Data.Person CreatePerson(string name)
        {
            var person = new Data.Person();

            person.Name = name;

            return person;
        }


        private void button1_Click(object sender, EventArgs e)
        {
            var person = CreatePerson("Bobb");
            var car = CreateCar("Sports car", "Lamborgini Aventador", "Dark blue", 2011, person);
            MessageBox.Show(car.Getdisplay());
        }
    }
}
