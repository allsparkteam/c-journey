﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace c_journey.Objects.Data
{
    class Mouse : Animal, IMouse 
    {
        public string MakeSound()
        {
            return string.Empty;
        }

        public override string Sound { get => "Squeak"; }
        public override string WrongOne()
        {
            return FirstWrong;
        }
    }
}
