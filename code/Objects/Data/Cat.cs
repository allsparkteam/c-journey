﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace c_journey.Objects.Data
{
    class Cat : Animal, ICat
    {

        public string MakeSound()
        {
            return "Thats right!";
        }

        public override string Sound { get => "Meow";}
        public override string GetName()
        {
            return Name;
        }

        public override string WrongOne()
        {
            return FirstWrong;
        }

    }
}
