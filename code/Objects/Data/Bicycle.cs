﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace c_journey.Objects.Data
{
   public class Bicycle : Vehicle
    {
        public string Maker { get; set; }
        public int AverageSpeed { get; set; }
        public string Tire { get; set; }

        public override string Getdisplay()
        {
            return Brand + Maker;
        }

    }

}

