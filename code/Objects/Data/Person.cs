﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace c_journey.Objects.Data
{
    public class Person
    {
        public string Name { get; set; }
        public string Occupation { get; set; }
        public int YearBorn { get; set; }
        public string From { get; set; }


    }
}
