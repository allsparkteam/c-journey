﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace c_journey.Objects.Data
{

   public class Car : Vehicle
    {
        public string Model { get; set; }       
        public int YearBuilt { get; set; }

        public override string Getdisplay()
        {

            // return  Brand + Color;


            return $"{base.Getdisplay()} Built in {YearBuilt}";

           
        }
    }
  
}
 