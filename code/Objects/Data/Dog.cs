﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace c_journey.Objects.Data
{
    class Dog : Animal, IDog
    {
        public string MakeSound()
        {
            return string.Empty;
        }
        public override string Sound { get => "Woof"; }

        public override string WrongOne()
        {
            return FirstWrong;
        }
    }
}
