﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace c_journey.Objects.Data
{
    abstract class Animal
    {
        public string Name { get; set; }
        public string FirstWrong { get; set; }
        public string SecondWrong { get; set; }

        public abstract string Sound { get;}
        

        public string CheckSound(string quizSound)
        {
            if (Sound.ToUpper() == quizSound.ToUpper())
            {
                return $"{this.GetType().Name} does indeed {quizSound}! ";
            }
            return $"{this.GetType().Name} does not {quizSound}!";
        }
        public virtual string GetName()
        {
            return Name;
        }

        public virtual string WrongOne()
        {
            return FirstWrong;
        }

    }
}
