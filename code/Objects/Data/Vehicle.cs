﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace c_journey.Objects.Data
{
    public class Vehicle
    {
        public string Brand { get; set; }
        public string Color { get; set; }
        public string Type { get; set; }
        public Person Owner { get; set; }

        public virtual string Getdisplay()
        {
            return $"This is a {Color} {Brand} of type {Type} owned by {Owner.Name}.";
        }
    }

}
