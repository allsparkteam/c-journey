﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace c_journey.Objects
{
    public partial class Animals : Form
    {
        public Animals()
        {
            InitializeComponent();
        }



        private void btnCatSound1_Click(object sender, EventArgs e)
        {
            var cat = CatWrongSound("That is incorrect");

            txtWrongCatSound.Text = cat.FirstWrong;
           
        }

        private Data.Cat CatWrongSound(string firstwrong)
        {
            var cat = new Data.Cat();
            cat.FirstWrong = firstwrong;        

            return cat;
        }



        private void btnDogRightAnswer_Click(object sender, EventArgs e)
        {
            var dog = DogWrongSound("That is wrong");

            txtWrongDogSound.Text = dog.FirstWrong;
        }

        private Data.Dog DogWrongSound(string firstwrong)
        {
            var dog = new Data.Dog();
            dog.FirstWrong = firstwrong;

                return dog;
        }

        private void btnMouseWrongSound_Click(object sender, EventArgs e)
        {
            var mouse = MouseWrongSound("That is wrong");

            txtWrongMouseSound.Text = mouse.FirstWrong;
        }

        private Data.Mouse MouseWrongSound(string firstwrong)
        {
            var mouse = new Data.Mouse();
            mouse.FirstWrong = firstwrong;

                return mouse;
        }




        // First and right Dog answer.
        private void btnRightDogAnswer_Click(object sender, EventArgs e)
        {
            var dog = Dogright("Thats right!");

            txtRightDogAnswer.Text = dog.Name;
        }

        private Data.Dog Dogright(string name)
        {
            var dog = new Data.Dog();
            dog.Name = name;

            return dog;
        }


        // Second Dog answer, its incorrect.
        private void btnWrongDogAnswer2_Click(object sender, EventArgs e)
        {
            var dog = DogWrongAnswer("Strange dog");

            txtWrongDogAnswer2.Text = dog.FirstWrong;
        }

        private Data.Dog DogWrongAnswer(string firstwrong)
        {
            var dog = new Data.Dog();
            dog.FirstWrong = firstwrong;

            return dog;
        }

        // Third Dog answer, its not right.
        private void btnWrongDogAnswer3_Click(object sender, EventArgs e)
        {
            var dog = DogSecondWrongAnswer("Nope!");

            txtWrongDogAnswer3.Text = dog.FirstWrong;
        }

        private Data.Dog DogSecondWrongAnswer(string firstwrong)
        {
            var dog = new Data.Dog();
            dog.FirstWrong = firstwrong;

            return dog;
        }

       

        // The third and right Mouse answer.
        private void btnMouseRightAnswer_Click(object sender, EventArgs e)
        {
            var mouse = MouseRightAnswer("Thats it!");

            txtRightMouseAnswer.Text = mouse.Name;
        }

        private Data.Mouse MouseRightAnswer(string name)
        {
            var mouse = new Data.Mouse();
            mouse.Name = name;

            return mouse;
        }

        // Second Mouse answer, its incorrect.
        private void btnWrongMouseAnswer2_Click(object sender, EventArgs e)
        {
            var mouse = MouseSecondWrongAnswer("Incorrect");

            txtWrongMouseAnswer2.Text = mouse.FirstWrong;
        }

        private Data.Mouse MouseSecondWrongAnswer(string firstWrong)
        {
            var mouse = new Data.Mouse();
            mouse.FirstWrong = firstWrong;

            return mouse;
        }

        // First mouse answer, it is incorrect.
        private void btnWrongMouseAnswer_Click(object sender, EventArgs e)
        {
            var mouse = MouseWrongAnswer("Wrong!");

            txtWrongMouseAnswer.Text = mouse.FirstWrong;
        }

        private Data.Mouse MouseWrongAnswer(string firstWrong)
        {
            var mouse = new Data.Mouse();
            mouse.FirstWrong = firstWrong;

            return mouse;
        }



        // third and final cat answer, this one is right.
        private void btnRightCatAnswer_Click(object sender, EventArgs e)
        {
            var cat = CatRightAnswer("Yes!");

            txtRightCatAnswer.Text = cat.Name;
        }

       private Data.Cat CatRightAnswer(string name)
        {
            var cat = new Data.Cat();
            cat.Name = name;

            return cat;
        }

        // second cat answer, this one is also wrong
        private void btnWrongCatAnswer2_Click(object sender, EventArgs e)
        {

            var cat = CatWrongAnswerTwo("That is wrong");

            txtWrongCatAnswer2.Text = cat.FirstWrong;
        }

        private Data.Cat CatWrongAnswerTwo(string firstwrong)
        {
            var cat = new Data.Cat();
            cat.FirstWrong = firstwrong;

            return cat;
        }

        // First cat answer, it is wrong.
        private void btnWrongCatAnswer_Click(object sender, EventArgs e)
        {
            var cat = CatWrongAnswer("Rightnt");

            txtWrongCatAnswer.Text = cat.FirstWrong;
        }

        private Data.Cat CatWrongAnswer(string firstwrong)
        {
            var cat = new Data.Cat();
            cat.FirstWrong = firstwrong;

            return cat;
        }

        private void btnRightCatSound_Click(object sender, EventArgs e)
        {       
                var catRight = CatRightSound("Thats Right!");

                txtRightCatSound.Text = catRight.MakeSound();

            
        }

        private Data.Cat CatRightSound(string makesound)
        {
            var catRight = new Data.Cat();
            //catRight.MakeSound = makesound;

            return catRight;
        }

        private void btnRightDogSound_Click(object sender, EventArgs e)
        {
            var dogRight = DogRightSound("Correct");

            txtRightDogSound.Text = dogRight.MakeSound();
        }

        private Data.Dog DogRightSound(string makesound)
        {
            var dogRight = new Data.Dog();
           // dogRight.MakeSound = makesound;

            return dogRight;
        }

        private void btnRightMouseSound_Click(object sender, EventArgs e)
        {
            var mouseRight = MouseRightSound("Yup");

            txtRightMouseSound.Text = mouseRight.MakeSound();
        }

        private Data.Mouse MouseRightSound(string makesound)
        {
            var mouseRight = new Data.Mouse();
           // mouseRight.MakeSound = makesound;

            return mouseRight;
        }

    }
}
