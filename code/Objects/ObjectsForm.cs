﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace c_journey.Objects
{
    public partial class ObjectsForm : Form
    {
        public ObjectsForm()
        {
            InitializeComponent();
        }

        private void btnNewCar_Click(object sender, EventArgs e)
        {
            var person = CreatePerson("Sakichi Toyoda", "Founder of Toyota Motor corp", "Japan", 1867);

            var car = CreateCar("Subcompact", "Toyota", "Yaris", "White", 1999, person);
         
            txtType.Text = car.Type;
            txtMaker.Text = car.Brand;
            txtModel.Text = car.Model;
            txtColor.Text = car.Color;
            numYearBuilt.Value = car.YearBuilt;

            
           txtName.Text = car.Owner.Name;
           txtOccupation.Text = car.Owner.Occupation;
           txtFrom.Text = car.Owner.From;
           numYearBorn.Value = car.Owner.YearBorn;

        }

        private Data.Car CreateCar(string type, string maker, string model, string color, int yearbuilt, Data.Person owner)
        {
            var car = new Data.Car();
            car.Type = type;
            car.Brand = maker;
            car.Model = model;
            car.Color = color;
            car.YearBuilt = yearbuilt;
            car.Owner = owner;
            return car;
        }

        private Data.Person CreatePerson(string name, string occupation, string from, int yearborn)
        {
            var person = new Data.Person();
            person.Name = name;
            person.Occupation = occupation;
            person.From = from;
            person.YearBorn = yearborn;
            return person;
        }

    }
}
