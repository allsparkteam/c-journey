﻿
namespace c_journey
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnObjects = new System.Windows.Forms.Button();
            this.btnVehicles = new System.Windows.Forms.Button();
            this.btnAnimals = new System.Windows.Forms.Button();
            this.btnOpenAnimalsAlternative = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnObjects
            // 
            this.btnObjects.Location = new System.Drawing.Point(49, 45);
            this.btnObjects.Name = "btnObjects";
            this.btnObjects.Size = new System.Drawing.Size(75, 23);
            this.btnObjects.TabIndex = 0;
            this.btnObjects.Text = "Objects";
            this.btnObjects.UseVisualStyleBackColor = true;
            this.btnObjects.Click += new System.EventHandler(this.btnObjects_Click);
            // 
            // btnVehicles
            // 
            this.btnVehicles.Location = new System.Drawing.Point(130, 45);
            this.btnVehicles.Name = "btnVehicles";
            this.btnVehicles.Size = new System.Drawing.Size(75, 23);
            this.btnVehicles.TabIndex = 1;
            this.btnVehicles.Text = "Vehicles";
            this.btnVehicles.UseVisualStyleBackColor = true;
            this.btnVehicles.Click += new System.EventHandler(this.btnVehicles_Click);
            // 
            // btnAnimals
            // 
            this.btnAnimals.Location = new System.Drawing.Point(211, 45);
            this.btnAnimals.Name = "btnAnimals";
            this.btnAnimals.Size = new System.Drawing.Size(75, 23);
            this.btnAnimals.TabIndex = 2;
            this.btnAnimals.Text = "Animals";
            this.btnAnimals.UseVisualStyleBackColor = true;
            this.btnAnimals.Click += new System.EventHandler(this.btnAnimals_Click);
            // 
            // btnOpenAnimalsAlternative
            // 
            this.btnOpenAnimalsAlternative.Location = new System.Drawing.Point(211, 74);
            this.btnOpenAnimalsAlternative.Name = "btnOpenAnimalsAlternative";
            this.btnOpenAnimalsAlternative.Size = new System.Drawing.Size(75, 23);
            this.btnOpenAnimalsAlternative.TabIndex = 3;
            this.btnOpenAnimalsAlternative.Text = "Animals Alt.";
            this.btnOpenAnimalsAlternative.UseVisualStyleBackColor = true;
            this.btnOpenAnimalsAlternative.Click += new System.EventHandler(this.btnOpenAnimalsAlternative_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(341, 124);
            this.Controls.Add(this.btnOpenAnimalsAlternative);
            this.Controls.Add(this.btnAnimals);
            this.Controls.Add(this.btnVehicles);
            this.Controls.Add(this.btnObjects);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "C# - Anakin\'s Journey";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnObjects;
        private System.Windows.Forms.Button btnVehicles;
        private System.Windows.Forms.Button btnAnimals;
        private System.Windows.Forms.Button btnOpenAnimalsAlternative;
    }
}

